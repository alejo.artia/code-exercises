# Given some integer, ﬁnd the maximal number you can obtain by deleting exactly one digit of the given
# number.
# Example
# For n = 152, the output should be
# deleteDigit(n) = 52;
# For n = 1001, the output should be
# deleteDigit(n) = 101.

def max_number(number):
    maximum = str(number)
    ret_num = 0

    for x in range(len(maximum)):
        new_num =  int(f'{maximum[:x]}{maximum[x+1:]}')
        if new_num > ret_num:
            ret_num = new_num
    print(ret_num)


max_number(51233)
