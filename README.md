# Code-Exercises

Here I want to resolve 1000 code exercises in python !

### -> exercise 1
Given some integer, ﬁnd the maximal number you can obtain by deleting exactly one digit of the given number.
Example:
For n = 152, the output should be deleteDigit(n) = 52;
For n = 1001, the output should be deleteDigit(n) = 101.

